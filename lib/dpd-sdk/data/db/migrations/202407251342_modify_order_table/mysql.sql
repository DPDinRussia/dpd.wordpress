ALTER TABLE b_ipol_dpd_order ADD COLUMN OBR char(1) not null default 'N';
ALTER TABLE b_ipol_dpd_order ADD COLUMN CHST char(4) null;
ALTER TABLE b_ipol_dpd_order ADD COLUMN GOODS_RETURN_AMOUNT decimal(10,5) null;
ALTER TABLE b_ipol_dpd_order ADD COLUMN DELIVERY_AMOUNT decimal(10,5) null;